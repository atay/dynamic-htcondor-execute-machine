#!/bin/bash

#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mail-type=FAIL,END
#SBATCH --output=/tmp/job-%x-%j-%N.out
#SBATCH --mail-user=atay@compeng.uni-frankfurt.de

source /lustre/loewecsc/condor/condor_build/condor.sh
/lustre/loewecsc/condor/condor_build/sbin/condor_master


sleep infinity
wait
