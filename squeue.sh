#!/bin/bash

slurm_nodes=$1
#check if slurm_list exists otherwise delete it
if [[ -a ${slurm_nodes} ]]; then
  rm ${slurm_nodes}
fi

#iterate over active slurm jobs and output the node hostname and slurm job ID into <slurm_nodes>
sjobs=0
while read line; do
    if [[ $sjobs == 0 ]]; then
	sjobs=$(($sjobs + 1))
    else
        sjobs=$(($sjobs + 1))
	sen2words=( $line )
	echo ${sen2words[7]} ${sen2words[0]}>>${slurm_nodes}
    fi
done
sjobs=$(($sjobs - 1))
